package pp2_gradle2;

import org.junit.Test;
import static org.junit.Assert.*;

public class Library2Test {
	
    Library2 testLibrary2 = new Library2();

	   @Test public void testSumaEnteros() {       	        
	        assertEquals("2+3 must be 5", 5, testLibrary2.sumaEnteros(2,3));
	   }
	   
	   @Test public void testprodEnteros() {
	        assertEquals("2*4 must be 8", 8, testLibrary2.prodEnteros(2, 4));
	   }    
	
}
